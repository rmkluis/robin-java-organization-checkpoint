package com.galvanize;

import formatters.Formatter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static com.galvanize.Application.getFormatter;
import static com.galvanize.Application.main;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ApplicationTest {
    Booking booking;
    PrintStream original;
    ByteArrayOutputStream outContent;

    // This block captures everything written to System.out
    @BeforeEach
    public void setOut() {
        original = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        booking = Booking.parse("c88888-1:32pm-2:03pm");
    }

    // This block resets System.out to whatever it was before
    @AfterEach
    public void restoreOut() {
        System.setOut(original);
    }

    @Test
    public void aTest_HTML() {
        String expected = "<dl>\n" +
                "  <dt>Type</dt><dd>Auditorium</dd>\n" +
                "  <dt>Room Number</dt><dd>13</dd>\n" +
                "  <dt>Start Time</dt><dd>08:30AM</dd>\n" +
                "  <dt>End Time</dt><dd>10:30AM</dd>\n" +
                "</dl>";
        Booking trip = Booking.parse("a13-08:30AM-10:30AM");
        Formatter actual = getFormatter("html");
        assertEquals(expected,actual.format(trip));
    }
    @Test
    public void aTest_CSV() {
        String expected = "type,room number,start time,end time\n" +
                "Auditorium,13,08:30AM,10:30AM";
        Booking trip = Booking.parse("a13-08:30AM-10:30AM");
        Formatter actual = getFormatter("csv");
        assertEquals(expected,actual.format(trip));
    }
    @Test
    public void aTest_JSON() {
        String expected = "{\n" +
                "  \"type\": \"Auditorium\",\n" +
                "  \"roomNumber\": 13,\n" +
                "  \"startTime\": \"08:30AM\",\n" +
                "  \"endTime\": \"10:30AM\"\n" +
                "}";
        Booking trip = Booking.parse("a13-08:30AM-10:30AM");
        Formatter actual = getFormatter("json");
        assertEquals(expected,actual.format(trip));
    }
    @Test
    public void testMain(){
        String expected = "type,room number,start time,end time\n" +
                "Auditorium,111,08:30AM,11:00AM\n";
        main(new String[]{"a111-08:30AM-11:00AM", "csv"});
        String actual = outContent.toString();
        assertEquals(expected, actual);
    }
}