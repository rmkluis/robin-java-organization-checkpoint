package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookingTest {
    Booking booking;
    @BeforeEach
    void setUp(){

    }

    @Test
    void parse() {
        //arrange/
        //act/
        //assert
        Booking expected = new Booking(Booking.RoomType.r,"111","8:30am","10:30am");
        Booking actual = Booking.parse("r111-8:30am-10:30am");
        assertEquals(expected.getEndTime(), actual.getEndTime());
        assertEquals(expected.getStartTime(), actual.getStartTime());
        assertEquals(expected.getRoomNumber(),actual.getRoomNumber());
        assertEquals(expected.getRoomType(),actual.getRoomType());
    }

    @Test
    void getRoomType() {
        booking = new Booking(Booking.RoomType.r,"111","8:30AM","10:30AM");
        String expected = "Conference Room";
        String actual = booking.getRoomType();
        assertEquals(expected,actual);
    }

    @Test
    void getRoomNumber() {
        booking = new Booking(Booking.RoomType.r,"111","8:30AM","10:30AM");
        String expected = "111";
        String actual = booking.getRoomNumber();
        assertEquals(expected,actual);
    }

    @Test
    void getStartTime() {
        booking = new Booking(Booking.RoomType.r,"111","8:30AM","10:30AM");
        String expected = "8:30AM";
        String actual = booking.getStartTime();
        assertEquals(expected,actual);
    }

    @Test
    void getStopTime() {
        booking = new Booking(Booking.RoomType.r,"111","8:30AM","10:30AM");
        String expected = "10:30AM";
        String actual = booking.getEndTime();
        assertEquals(expected,actual);
    }
}