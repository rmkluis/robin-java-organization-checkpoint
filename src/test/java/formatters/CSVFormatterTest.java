package formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CSVFormatterTest {
    CSVFormatter csvFormatter;
    Booking booking;
    @BeforeEach
    void setUp(){
        csvFormatter = new CSVFormatter();
        booking = Booking.parse("c88888-1:32pm-2:03pm");
    }
    @Test
    void format() {
        assertEquals("type,room number,start time,end time\n"+booking.getRoomType()+","+booking.getRoomNumber()+","+booking.getStartTime()+","+booking.getEndTime(), csvFormatter.format(booking));
    }
}