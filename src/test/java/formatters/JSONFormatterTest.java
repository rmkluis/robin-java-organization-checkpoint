package formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.galvanize.Application.getFormatter;
import static org.junit.jupiter.api.Assertions.*;

class JSONFormatterTest {
    JSONFormatter jsonFormatter;
    @BeforeEach
    void setUp(){
        jsonFormatter = new JSONFormatter();
    }
    @Test
    void format() {
        String expected = "{\n" +
                "  \"type\": \"Auditorium\",\n" +
                "  \"roomNumber\": 13,\n" +
                "  \"startTime\": \"08:30AM\",\n" +
                "  \"endTime\": \"10:30AM\"\n" +
                "}";
        Booking trip = Booking.parse("a13-08:30AM-10:30AM");
        Formatter actual = getFormatter("json");
        assertEquals(expected,actual.format(trip));
    }
}