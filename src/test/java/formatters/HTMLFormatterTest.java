package formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HTMLFormatterTest {
    HTMLFormatter htmlFormatter;
    Booking booking;
    @BeforeEach
    void setUp(){
        htmlFormatter = new HTMLFormatter();
        booking = Booking.parse("c88888-1:32pm-2:03pm");
    }
    @Test
    void format() {
        assertEquals(String.format("<dl>\n  <dt>Type</dt><dd>%s</dd>\n  <dt>Room Number</dt><dd>%s</dd>\n  <dt>Start Time</dt><dd>%s</dd>\n  <dt>End Time</dt><dd>%s</dd>\n</dl>",booking.getRoomType(),booking.getRoomNumber(),booking.getStartTime(),booking.getEndTime()), htmlFormatter.format(booking));
    }
}