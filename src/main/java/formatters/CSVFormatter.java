package formatters;

import com.galvanize.Booking;

public class CSVFormatter implements Formatter{
    public String format(Booking booking){
        return "type,room number,start time,end time\n"+booking.getRoomType()+","+booking.getRoomNumber()+","+booking.getStartTime()+","+booking.getEndTime();
    }
}
