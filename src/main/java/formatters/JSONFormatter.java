package formatters;

import com.galvanize.Booking;

public class JSONFormatter implements Formatter{
    public String format(Booking booking) {
        return String.format("{\n  \"type\": \"%s\",\n  \"roomNumber\": %s,\n  \"startTime\": \"%s\",\n  \"endTime\": \"%s\"\n}", booking.getRoomType(),booking.getRoomNumber(),booking.getStartTime(),booking.getEndTime());
    }
}
