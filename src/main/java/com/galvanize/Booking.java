package com.galvanize;

public class Booking {
    private String roomType;
    private String roomNumber;
    private String startTime;
    private String endTime;
    enum RoomType{r,s,a,c}

    public static Booking parse(String bookingCode){
        String[] codeSplit = bookingCode.split("-");
        return new Booking(RoomType.valueOf(codeSplit[0].split("")[0]), codeSplit[0].substring(1), codeSplit[1], codeSplit[2]);
    }
    public Booking(RoomType rType, String rNum, String sTime, String eTime){
        switch (rType){
            case a : roomType = "Auditorium"; break;
            case c : roomType = "Classroom"; break;
            case r : roomType = "Conference Room"; break;
            case s : roomType = "Suite";
        }
        roomNumber=rNum;
        startTime=sTime;
        endTime=eTime;
    }
    public String getRoomType(){
        return roomType;
    }
    public String getRoomNumber(){
        return roomNumber;
    }
    public String getStartTime(){
        return startTime;
    }
    public String getEndTime() {
        return endTime;
    }
}
