package com.galvanize;

import formatters.*;

public class Application {
    public static Formatter getFormatter(String str){
         switch (str.toLowerCase()) {
             case "html" : return new HTMLFormatter();
             case "json" : return new JSONFormatter();
             case "csv" : return new CSVFormatter();
        }
         return null;
    }
    public static void main(String[] args) {
        Booking booking = Booking.parse(args[0]);
        Formatter formatter = getFormatter(args[1]);
        System.out.println(formatter.format(booking));

//        Booking booking = Booking.parse(        "a13-08:30AM-10:30AM");
//        Formatter formatter = getFormatter("json");
//        System.out.println(formatter.format(booking));

    }

}